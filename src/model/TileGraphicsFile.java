package model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import javafx.util.Pair;
import model.game.TileType;
import util.Matrix;
import util.Vector;

public class TileGraphicsFile {

	private static final TileGraphicsFile GRAPHICS_FILE = new TileGraphicsFile(new File("resources/constants.json"), new File("resources/tileFiles.json"), true);

	private static final String JSON_MEEPLE = "meeple";
	private static final String JSON_ROAD = "road";
	private static final String JSON_CITY = "city";
	private static final String JSON_SHIELD = "shield";
	private static final String JSON_MONASTERY = "monastery";
	private static final String JSON_GRASS = "grass";

	private HashMap<Pair<TileType, JsonGraphicType>, ArrayList<Matrix>> readTileSubtypes;
	private HashMap<String, Double> readConstants;
	private HashMap<String, Matrix> readMatrixConstants;
	private JsonNode constantsRoot;
	private JsonNode matrixConstantsRoot;
	private boolean readonly;

	private JsonNode lookupFileRoot;

	private TileGraphicsFile(File constantsFile, File tileLookupFile, boolean readonly) {
		readTileSubtypes = new HashMap<>();
		readConstants = new HashMap<>();
		readMatrixConstants = new HashMap<>();
		constantsRoot = null;
		matrixConstantsRoot = null;
		try {
			constantsRoot = (new ObjectMapper()).readTree(constantsFile);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("File " + constantsFile.getPath() + " is not a correctly formed JSON file.", e);
		} catch (IOException e) {
			throw new RuntimeException("Error reading file " + constantsFile.getPath() + ".", e);
		}

		try {
			lookupFileRoot = (new ObjectMapper()).readTree(tileLookupFile);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("JsonProcessingException handler not yet written in TileGraphicsFile of TileGraphicsFile.", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in TileGraphicsFile of TileGraphicsFile.", e);
		}
	}

	public static TileGraphicsFile getTileGraphicsFile() {
		return GRAPHICS_FILE;
	}

	public ArrayList<Matrix> getTileMatrix(TileType tileType, JsonGraphicType matrixName) throws IOException {
		if (tileType == null || matrixName == null) {
			throw new NullPointerException();
		}
		Pair<TileType, JsonGraphicType> key = new Pair<>(tileType, matrixName);

		if (readTileSubtypes.containsKey(key)) {
			return readTileSubtypes.get(key);
		}

		// Find this tile in the lookup file. The lookup file will have the path to the tile file.
		JsonNode tileFileNameNode = lookupFileRoot.get(tileType.toString());
		if (tileFileNameNode == null || tileFileNameNode.getNodeType() != JsonNodeType.STRING) {
			throw new IOException("No tile file lookup entry found for tile \"" + tileType.toString() + "\".");
		}

		// Find the tile file.
		String tileFileName = tileFileNameNode.textValue();
		File tileFile = new File(tileFileName);
		if (!tileFile.exists()) {
			throw new IOException("Tile file \"" + tileFileName + "\" not found for tile \"" + tileType + "\".");
		}

		// Read the tile file.
		JsonNode tileRoot;
		try {
			tileRoot = (new ObjectMapper()).readTree(tileFile);
		} catch (JsonProcessingException e) {
			throw new IOException("handler not yet written", e);
		} catch (IOException e) {
			throw new IOException("handler not yet written", e);
		}
		if (tileRoot == null || !tileRoot.isObject()) {
			throw new IOException();
		}

		// Get the graphics field of the tile file.
		JsonNode graphicsNode = tileRoot.get("graphics");
		if (graphicsNode == null || !graphicsNode.isObject()) {
			throw new IOException();
		}

		// Parse this the usual way.
		ArrayList<Matrix> ret = readTileGraphics(graphicsNode, matrixName);
		readTileSubtypes.put(key, ret);
		return ret;
	}

	public ArrayList<Matrix> readTileGraphics(JsonNode tileRoot, JsonGraphicType matrixName) throws IOException {
		if (tileRoot == null || !tileRoot.isObject()) {
			throw new IOException("Expected tile object, found: \"" + tileRoot.toString() + "\".");
		}

		JsonNode matrixArray = tileRoot.get(matrixName.toString());
		ArrayList<Matrix> ret = new ArrayList<>();
		if (matrixArray != null) {
			if (!matrixArray.isArray()) {
				throw new IOException("Expected array of matricies for entry \"" + matrixName + "\".");
			}

			try {
				for (JsonNode matrixNode : matrixArray) {
					Matrix matrix = readMatrixJson(matrixNode);
					if (readonly) {
						matrix.lock();
					}
					ret.add(matrix);
				}
			} catch (IOException e) {
				throw new IOException("Error while parsing matrix for entry \"" + matrixName + "\".", e);
			}
		}

		return ret;
	}

	private Matrix readMatrixJson(JsonNode matrixNode) throws IOException {
		if (matrixNode == null) {
			throw new IOException("Matrix entry is not an array.");
		}
		if (matrixNode.getNodeType() == JsonNodeType.STRING) {
			return getMatrixConstant(matrixNode.textValue());
		}
		ArrayList<Vector> vectors = new ArrayList<>();
		for (JsonNode vectorNode : matrixNode) {
			vectors.add(readVectorJson(vectorNode));
		}

		return new Matrix(vectors.toArray(new Vector[0]));
	}

	private Vector readVectorJson(JsonNode vectorNode) throws IOException {
		if (vectorNode == null || !vectorNode.isArray()) {
			throw new IOException("Matrix entry contains a non-array entry.");
		}
		if (vectorNode.size() != 2) {
			if (vectorNode.size() > 2) {
				throw new IOException("Matrix entry contains a vector with more than 2 dimentions.");
			} else {
				throw new IOException("Matrix entry contains a vector with less than 2 dimensions.");
			}
		}

		return new Vector(readNumberJson(vectorNode.get(0)), readNumberJson(vectorNode.get(1)));
	}

	private double readNumberJson(JsonNode numberNode) throws IOException {
		if (numberNode != null && numberNode.isNumber()) {
			return numberNode.doubleValue();
		} else if (numberNode == null || numberNode.getNodeType() != JsonNodeType.STRING) {
			throw new IOException("Number or constant name expected.");
		}

		String constantName = numberNode.asText();

		return getConstant(constantName);
	}

	private double getConstant(String name) throws IOException {
		if (readConstants.containsKey(name)) {
			return readConstants.get(name);
		}

		JsonNode numericConstantsRoot = constantsRoot.get("constants");
		if (numericConstantsRoot == null || !constantsRoot.isObject()) {
			throw new IOException("Cannot find constant \"" + name + "\": No constants defined.");
		}

		JsonNode constantNode = numericConstantsRoot.get(name);
		if (constantNode == null) {
			throw new IOException("Cannot find constant \"" + name + "\".");
		} else if (!constantNode.isNumber()) {
			throw new IOException("The constant \"" + name + "\" is not a number.");
		}

		double ret = constantNode.doubleValue();
		readConstants.put(name, ret);
		return ret;
	}

	private Matrix getMatrixConstant(String name) throws IOException {
		if (readMatrixConstants.containsKey(name)) {
			return readMatrixConstants.get(name);
		}

		if (matrixConstantsRoot == null) {
			matrixConstantsRoot = constantsRoot.get("matrices");
			if (matrixConstantsRoot == null || !matrixConstantsRoot.isObject()) {
				throw new IOException("Cannot find constant matrix \"" + name + "\": No matrix constans defined.");
			}
		}

		JsonNode matrixConstantNode = matrixConstantsRoot.get(name);
		if (matrixConstantNode == null) {
			throw new IOException("Cannot find matrix constant \"" + name + "\".");
		}

		Matrix ret = readMatrixJson(matrixConstantNode);
		readMatrixConstants.put(name, ret);
		return ret;
	}

	public static enum JsonGraphicType {
		MEEPLE, ROAD, CITY, MONASTERY, GRASS, SHIELD;

		@Override
		public String toString() {
			switch (this) {
				case MEEPLE:
					return JSON_MEEPLE;
				case ROAD:
					return JSON_ROAD;
				case CITY:
					return JSON_CITY;
				case MONASTERY:
					return JSON_MONASTERY;
				case GRASS:
					return JSON_GRASS;
				case SHIELD:
					return JSON_SHIELD;
				default:
					throw new IllegalStateException();
			}
		}
	}
}
