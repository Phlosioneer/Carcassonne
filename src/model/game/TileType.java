package model.game;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import model.TileEdgesFile;
import model.game.Tile.TileEdgeContent;
import model.game.Tile.TileSide;

public enum TileType {

	EMPTY,

	// Default orientation: Up and Right sides.
	ROAD_CORNER,

	// Default orientation: Up and Down sides.
	ROAD_STRAIGHT,

	// Default orientation: Up, Right, and Down sides.
	ROAD_T,

	// Default orientation: n/a.
	ROAD_CROSS,

	// Default orientation: Up side.
	CITY_ONE,

	// Default orientation: Up and Right side.
	CITY_CORNER_CONNECTED, CITY_CORNER_CONNECTED_SHIELD, CITY_CORNER_DISCONNECTED,

	// Default orientation: Up, Right, and Down sides.
	CITY_T, CITY_T_SHIELD,

	// Default orientation: Up and Down sides.
	CITY_OPPOSITE_CONNECTED, CITY_OPPOSITE_CONNECTED_SHIELD, CITY_OPPOSITE_DISCONNECTED,

	// Default orientation: n/a.
	CITY_FULL,

	CITY_CORNER_WITH_ROAD, CITY_CORNER_WITH_ROAD_SHIELD,

	CITY_T_WITH_ROAD, CITY_T_WITH_ROAD_SHIELD,

	CITY_WITH_ROAD_CORNER_RIGHT,

	CITY_WITH_ROAD_CORNER_LEFT,

	CITY_WITH_ROAD_STRAIGHT,

	CITY_WITH_ROAD_T,

	MONASTERY,

	MONASTERY_WITH_ROAD;

	public Map<TileSide, TileEdgeContent> getSides() {
		try {
			return TileEdgesFile.getTileEdgesFile().edgesForTile(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in getSides of TileType.", e);
		}
	}

	// Rotates clockwise, ROTATION times.
	public Map<TileSide, TileEdgeContent> getSides(int rotation) {
		return rotate(getSides(), rotation);
	}

	public static Map<TileSide, TileEdgeContent> rotate(Map<TileSide, TileEdgeContent> sides, int rotation) {
		if (rotation == 0) {
			return sides;
		} else if (rotation < 0) {
			throw new IllegalStateException();
		} else {
			Map<TileSide, TileEdgeContent> ret = new EnumMap<>(TileSide.class);
			Map<TileSide, TileEdgeContent> prev = rotate(sides, rotation - 1);
			ret.put(TileSide.UP, prev.get(TileSide.LEFT));
			ret.put(TileSide.RIGHT, prev.get(TileSide.UP));
			ret.put(TileSide.DOWN, prev.get(TileSide.RIGHT));
			ret.put(TileSide.LEFT, prev.get(TileSide.DOWN));
			return ret;
		}
	}
}