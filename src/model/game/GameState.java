package model.game;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.EnumMap;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import model.TileCountsFile;

public class GameState {

	private int playerOneScore;
	private int playerTwoScore;
	private Board board;
	private Queue<TileType> tileDeck;

	public GameState() {
		playerOneScore = 0;
		playerTwoScore = 0;
		board = new Board();
		tileDeck = new ArrayDeque<>();

		EnumMap<TileType, Integer> deckCounts = new EnumMap<>(TileType.class);
		try {
			for (TileType type : TileType.values()) {
				if (type == TileType.EMPTY) {
					continue;
				}
				deckCounts.put(type, TileCountsFile.geTileCountsFile().getTileCount(type));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in GameState of GameState.", e);
		}

		int totalCount = 0;
		for (int count : deckCounts.values()) {
			totalCount += count;
		}
		Random random = new Random();
		for (int i = totalCount; i > 0; i--) {
			int index = random.nextInt(i);
			TileType type = null;
			for (Map.Entry<TileType, Integer> pair : deckCounts.entrySet()) {
				if (pair.getValue() > index) {
					type = pair.getKey();
					break;
				}
				index -= pair.getValue();
			}

			if (type == null) {
				throw new IllegalStateException();
			}

			deckCounts.put(type, deckCounts.get(type) - 1);
			tileDeck.add(type);
		}

	}

	public int getPlayerOneScore() {
		return playerOneScore;
	}

	public int getPlayerTwoScore() {
		return playerTwoScore;
	}

	public Board getBoard() {
		return board;
	}

	public Queue<TileType> getTileDeck() {
		return tileDeck;
	}
}
