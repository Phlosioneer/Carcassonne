package model.game;

import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Board {

	// Row-storage. (outer array is an array of columns.)
	private ArrayList<ArrayList<Tile>> tiles;

	private IntegerProperty rowCount;
	private IntegerProperty columnCount;

	public Board() {
		// Start as a 1x1 board.
		tiles = new ArrayList<>();
		for (int i = 0; i != 1; i++) {
			ArrayList<Tile> row = new ArrayList<>();
			tiles.add(row);
			for (int j = 0; j != 1; j++) {
				row.add(makeTile());
			}
		}

		rowCount = new SimpleIntegerProperty(tiles.get(0).size());
		columnCount = new SimpleIntegerProperty(tiles.size());

		getTile(0, 0).setType(TileType.CITY_WITH_ROAD_STRAIGHT);
	}

	public Tile getTile(int x, int y) {
		return tiles.get(x).get(y);
	}

	public int getRowCount() {
		return rowCount.get();
	}

	public ReadOnlyIntegerProperty rowCountProperty() {
		return rowCount;
	}

	public int getColumnCount() {
		return columnCount.get();
	}

	public ReadOnlyIntegerProperty columnCountProperty() {
		return columnCount;
	}

	private Tile makeTile() {
		Tile ret = new Tile();
		ret.typePorperty().addListener(Event->checkEdges());
		return ret;
	}

	private void checkEdges() {
		for (int i = 0; i != tiles.size(); i++) {
			if (tiles.get(i).get(0).getType() != TileType.EMPTY) {
				// Add a blank row.
				for (int j = 0; j != tiles.size(); j++) {
					tiles.get(j).add(0, makeTile());
				}

				rowCount.set(tiles.get(0).size());

				break;
			}
		}
		for (int i = 0; i != tiles.size(); i++) {
			if (tiles.get(i).get(tiles.get(i).size() - 1).getType() != TileType.EMPTY) {
				// Add a blank row.
				for (int j = 0; j != tiles.size(); j++) {
					tiles.get(j).add(makeTile());
				}

				rowCount.set(tiles.get(0).size());

				break;
			}
		}

		for (int i = 0; i != tiles.get(0).size(); i++) {
			if (tiles.get(0).get(i).getType() != TileType.EMPTY) {
				// Add a blank column.
				ArrayList<Tile> newColumn = new ArrayList<>();
				for (int j = 0; j != tiles.get(0).size(); j++) {
					newColumn.add(makeTile());
				}
				tiles.add(0, newColumn);

				columnCount.set(tiles.size());

				break;
			}
		}
		for (int i = 0; i != tiles.get(0).size(); i++) {
			if (tiles.get(tiles.size() - 1).get(i).getType() != TileType.EMPTY) {
				// Add a blank column.
				ArrayList<Tile> newColumn = new ArrayList<>();
				for (int j = 0; j != tiles.get(0).size(); j++) {
					newColumn.add(makeTile());
				}
				tiles.add(newColumn);

				columnCount.set(tiles.size());

				break;
			}
		}

		for (int i = 0; i != tiles.size(); i++) {
			ArrayList<Tile> column = tiles.get(i);
			for (int j = 0; j != column.size(); j++) {
				Tile tile = column.get(j);
				tile.setupConnections(this, i, j);
			}
		}
	}
}
