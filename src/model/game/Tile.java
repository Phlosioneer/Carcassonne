package model.game;

import java.text.ParseException;
import java.util.EnumMap;
import java.util.Map;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Tile {

	private ObjectProperty<TileType> type;
	private IntegerProperty rotation;
	private EnumMap<TileSide, Tile> neighbors;

	public Tile() {
		this(TileType.EMPTY, 0);
	}

	public Tile(TileType type) {
		this(type, 0);
	}

	protected Tile(TileType type, int rotation) {
		if (type == null) {
			throw new NullPointerException();
		}

		this.type = new SimpleObjectProperty<>(TileType.EMPTY);
		this.rotation = new SimpleIntegerProperty(rotation % 4);
		neighbors = new EnumMap<>(TileSide.class);

		this.type.set(type);
	}

	public ObjectProperty<TileType> typePorperty() {
		return type;
	}

	public TileType getType() {
		return type.get();
	}

	public void setType(TileType type) {
		if (type == null) {
			throw new NullPointerException();
		}
		this.type.set(type);
	}

	public TileEdgeContent getSideContent(TileSide side) {
		return type.get().getSides(rotation.get()).get(side);
	}

	public void rotate() {
		rotation.setValue((rotation.get() + 1) % 4);
	}

	public int getRotation() {
		return rotation.get();
	}

	public ReadOnlyIntegerProperty rotationProperty() {
		return rotation;
	}

	public void setupConnections(Board parent, int x, int y) {
		if (y > 0) {
			neighbors.put(TileSide.UP, parent.getTile(x, y - 1));
		} else {
			neighbors.put(TileSide.UP, null);
		}
		if (y < parent.getRowCount() - 1) {
			neighbors.put(TileSide.DOWN, parent.getTile(x, y + 1));
		} else {
			neighbors.put(TileSide.DOWN, null);
		}
		if (x > 0) {
			neighbors.put(TileSide.LEFT, parent.getTile(x - 1, y));
		} else {
			neighbors.put(TileSide.LEFT, null);
		}
		if (x < parent.getColumnCount() - 1) {
			neighbors.put(TileSide.RIGHT, parent.getTile(x + 1, y));
		} else {
			neighbors.put(TileSide.RIGHT, null);
		}
	}

	public boolean copyTile(Tile other) {
		Map<TileSide, TileEdgeContent> edges = other.getSides();

		boolean isAllEmpty = true;
		for (TileSide side : TileSide.values()) {
			Tile neighbor = neighbors.get(side);
			if (neighbor != null && neighbor.getType() != TileType.EMPTY) {
				isAllEmpty = false;
				break;
			}
		}
		if (isAllEmpty) {
			return false;
		}

		for (TileSide side : TileSide.values()) {
			Tile neighbor = neighbors.get(side);
			if (neighbor == null) {
				continue;
			}
			TileEdgeContent neighborContent = neighbor.getSideContent(side.oppositeSide());
			TileEdgeContent content = edges.get(side);

			if (!content.matches(neighborContent)) {
				return false;
			}
		}

		setType(other.getType());
		rotation.set(other.rotation.get());

		return true;
	}

	public Map<TileSide, TileEdgeContent> getSides() {
		return getType().getSides(getRotation());
	}

	@Override
	public String toString() {
		return "[Tile " + getType() + ", " + getRotation() + "]";
	}

	public Map<TileSide, Tile> getNeighbors() {
		return neighbors;
	}

	public static enum TileSide {
		UP, RIGHT, DOWN, LEFT;

		public TileSide oppositeSide() {
			switch (this) {
				case UP:
					return DOWN;
				case DOWN:
					return UP;
				case LEFT:
					return RIGHT;
				case RIGHT:
					return LEFT;
				default:
					throw new IllegalStateException();
			}
		}

		public static TileSide fromString(String sideString) throws ParseException {
			for (TileSide content : values()) {
				if (content.toString().equals(sideString)) {
					return content;
				}
			}

			throw new ParseException("Cannot map \"" + sideString + "\" to a valid TileSide value.", 0);
		}
	}

	public static enum TileEdgeContent {
		EMPTY, FARM, ROAD, CITY;

		public static TileEdgeContent fromString(String contentString) throws ParseException {
			for (TileEdgeContent content : values()) {
				if (content.toString().equals(contentString)) {
					return content;
				}
			}

			throw new ParseException("Cannot map \"" + contentString + "\" to a valid TileContent value.", 0);
		}

		public boolean matches(TileEdgeContent otherContent) {
			if (this == EMPTY || otherContent == EMPTY) {
				return true;
			}

			return this == otherContent;
		}
	}
}
