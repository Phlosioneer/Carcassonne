package model;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.game.TileType;

public class TileCountsFile {

	private static final TileCountsFile COUNTS_FILE = new TileCountsFile(new File("resources/tileCounts.json"));

	private HashMap<TileType, Integer> readTileCounts;
	private JsonNode root;

	private TileCountsFile(File file) {
		readTileCounts = new HashMap<>();
		try {
			root = (new ObjectMapper()).readTree(file);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("File " + file.getPath() + " is not a correctly formed JSON file.", e);
		} catch (IOException e) {
			throw new RuntimeException("Error reading file " + file.getPath() + ".", e);
		}
	}

	public static TileCountsFile geTileCountsFile() {
		return COUNTS_FILE;
	}

	public int getTileCount(TileType tileName) throws IOException {
		if (readTileCounts.containsKey(tileName)) {
			return readTileCounts.get(tileName);
		}

		JsonNode count = root.get(tileName.toString());
		if (count == null || !count.isInt()) {
			throw new IOException("Expected integer for entry \"" + tileName + "\".");
		}

		int ret = count.intValue();
		readTileCounts.put(tileName, ret);

		return ret;
	}
}
