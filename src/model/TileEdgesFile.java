package model;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.EnumMap;
import java.util.Map;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import model.game.Tile.TileEdgeContent;
import model.game.Tile.TileSide;
import model.game.TileType;

public class TileEdgesFile {

	private static final TileEdgesFile TILE_EDGES = new TileEdgesFile(new File("resources/tileEdges.json"), new File("resources/tileFiles.json"));

	private JsonNode lookupFileRoot;

	private TileEdgesFile(File edgesFile, File lookupFile) {
		try {
			lookupFileRoot = (new ObjectMapper()).readTree(lookupFile);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("JsonProcessingException handler not yet written in TileEdgesFile of TileEdgesFile.", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in TileEdgesFile of TileEdgesFile.", e);
		}
	}

	public static TileEdgesFile getTileEdgesFile() {
		return TILE_EDGES;
	}

	public Map<TileSide, TileEdgeContent> edgesForTile(TileType type) throws IOException {

		JsonNode tileFileNameNode = lookupFileRoot.get(type.toString());
		if (tileFileNameNode == null || tileFileNameNode.getNodeType() != JsonNodeType.STRING) {
			throw new IOException("No file entry found for tile \"" + type + "\".");
		}

		File tileFile = new File(tileFileNameNode.textValue());
		if (!tileFile.exists()) {
			throw new IOException("Could not find file \"" + tileFileNameNode.textValue() + "\" for tile \"" + type + "\".");
		}

		JsonNode tileFileRoot = (new ObjectMapper()).readTree(tileFile);
		if (tileFileRoot == null || !tileFileRoot.isObject()) {
			throw new IOException();
		}

		JsonNode edgesRoot = tileFileRoot.get("edges");
		if (edgesRoot == null || !edgesRoot.isObject()) {
			throw new IOException();
		}

		Map<TileSide, TileEdgeContent> ret;
		try {
			ret = readTileObject(edgesRoot);
		} catch (IOException e) {
			throw new IOException("Error while parsing edge object for tile " + type + ".", e);
		}

		return ret;
	}

	public Map<TileSide, TileEdgeContent> readTileObject(JsonNode edgesRoot) throws IOException {
		EnumMap<TileSide, TileEdgeContent> ret = new EnumMap<>(TileSide.class);

		for (TileSide side : TileSide.values()) {
			JsonNode sideNode = edgesRoot.get(side.toString());
			if (sideNode == null || sideNode.getNodeType() != JsonNodeType.STRING) {
				throw new IOException("Side entry " + side + " is not a string.");
			}

			String contentString = sideNode.textValue();
			TileEdgeContent edgeContent;
			try {
				edgeContent = TileEdgeContent.fromString(contentString);
			} catch (ParseException e) {
				throw new IOException("Error while parsing side entry " + side + ".", e);
			}
			ret.put(side, edgeContent);
		}

		return ret;
	}
}
