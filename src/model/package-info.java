
/**
 * 
 * Contains all the game logic, file system interaction, etc. It should not have any dependencies on the gui package.
 * 
 */
package model;