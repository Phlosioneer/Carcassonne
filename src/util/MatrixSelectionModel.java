package util;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public abstract class MatrixSelectionModel<T, I extends MatrixSelectionModel.MatrixIndex> {

	private ObjectProperty<I> selectedIndex;
	private ObjectProperty<T> selectedItem;

	public MatrixSelectionModel() {
		selectedIndex = new SimpleObjectProperty<>();
		selectedItem = new SimpleObjectProperty<>();
	}

	// Subclassing

	protected void setSelectedIndex(I index) {
		selectedIndex.set(index);
	}

	protected void setSelectedItem(T item) {
		selectedItem.set(item);
	}

	// Selection management.

	public abstract void clearSelection();

	public abstract void select(I index);

	public abstract void select(T item);

	public abstract void clearAndSelect(I index);

	public abstract void clearAndSelect(T item);

	public abstract boolean isEmpty();

	public boolean isSelected(MatrixIndex index) {
		return getSelectedIndex().equals(index);
	}

	public boolean isSelected(T item) {
		return getSelectedItem() == item;
	}

	// Getters.

	public ReadOnlyObjectProperty<T> selectedItemProperty() {
		return selectedItem;
	}

	public T getSelectedItem() {
		return selectedItem.get();
	}

	public ReadOnlyObjectProperty<I> selectedIndexProperty() {
		return selectedIndex;
	}

	public I getSelectedIndex() {
		return selectedIndex.get();
	}

	public static class MatrixIndex {
		protected int x;
		protected int y;

		protected MatrixIndex(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
}
