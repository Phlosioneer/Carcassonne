package util;

import java.util.HashMap;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;
import com.snatik.polygon.Polygon.Builder;

public class MatrixGeom {

	private static HashMap<Matrix, Polygon> cachedPolygons = new HashMap<>();

	private MatrixGeom() {
		throw new UnsupportedOperationException("Constructor for MatrixGeom not yet implemented.");
	}

	public static boolean matrixContainsPoint(Matrix matrix, double x, double y) {

		Polygon polygon = getPolygon(matrix);
		return polygon.contains(new Point(x, y));

	}

	public static Polygon getPolygon(Matrix matrix) {
		if (cachedPolygons.containsKey(matrix)) {
			return cachedPolygons.get(matrix);
		}

		Builder polygonBuilder = Polygon.Builder();
		for (Vector vector : matrix) {
			polygonBuilder.addVertex(new Point(vector.x, vector.y));
		}
		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		cachedPolygons.put(matrix, polygon);
		return polygon;
	}

	public static Vector centerPoint(Matrix matrix) {
		Vector total = new Vector();
		for (Vector current : matrix) {
			total.add(current);
		}

		total.div(matrix.size());
		return total;
	}
}
