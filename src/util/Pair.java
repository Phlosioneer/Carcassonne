package util;

public class Pair<A, B> {

	public A first;
	public B second;

	public Pair(A first, B second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair)) {
			return false;
		}
		Pair<?, ?> other = (Pair<?, ?>) obj;

		return first.equals(other.first) && second.equals(other.second);
	}

	@Override
	public int hashCode() {
		return first.hashCode() ^ second.hashCode();
	}
}
