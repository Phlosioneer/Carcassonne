package util;

public class Vector {

	public double x;
	public double y;

	public Vector() {
		this(0, 0);
	}

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vector(Vector other) {
		this(other.x, other.y);
	}

	public Vector mult(double scalar) {
		x *= scalar;
		y *= scalar;
		return this;
	}

	// https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions
	public Vector rotate(double radians) {
		double newX = x * Math.cos(radians) - y * Math.sin(radians);
		double newY = x * Math.sin(radians) + y * Math.cos(radians);
		x = newX;
		y = newY;
		return this;
	}

	public Vector add(Vector other) {
		x += other.x;
		y += other.y;
		return this;
	}

	public Vector sub(Vector other) {
		x -= other.x;
		y -= other.y;
		return this;
	}

	public static Vector mult(Vector vector, int scalar) {
		return new Vector(vector).mult(scalar);
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

	public void rotateAround(Vector vector, double d) {
		sub(vector);
		rotate(d);
		add(vector);
	}

	public void div(int scalar) {
		x /= scalar;
		y /= scalar;
	}
}
