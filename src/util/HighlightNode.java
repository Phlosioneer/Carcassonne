package util;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class HighlightNode {

	private HighlightNode() {

	}

	public static Rectangle highlightChildNode(Node child, Color color) {

		Rectangle rectangle = new Rectangle(0, 0, Color.TRANSPARENT);
		rectangle.setStroke(color);
		rectangle.setManaged(false);
		child.boundsInParentProperty().addListener((observable, oldVal, newVal)-> {
			rectangle.setLayoutX(child.getBoundsInParent().getMinX());
			rectangle.setLayoutY(child.getBoundsInParent().getMinY());
			rectangle.setWidth(child.getBoundsInParent().getWidth());
			rectangle.setHeight(child.getBoundsInParent().getHeight());
		});

		return rectangle;
	}
}
