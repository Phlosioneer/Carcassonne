
/**
 * A package for classes that do not fit into the {@link gui} or {@link model} packages.
 */
package util;