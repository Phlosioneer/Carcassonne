package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * 
 * An (incomplete) implementation of a 2 by N matrix.
 * 
 * @author Phlosioneer
 *
 */
public class Matrix implements Iterable<Vector> {

	private boolean locked;
	private ArrayList<Vector> columns;

	/**
	 * Create an empty matrix.
	 */
	public Matrix() {
		columns = new ArrayList<>();
		locked = false;
	}

	/**
	 * Create a matrix, using the given {@link util.Vector vectors} as columns. Takes ownership of the passed vectors.
	 * 
	 * @param columns
	 *            The starting values of the columns.
	 */
	public Matrix(Vector[] columns) {
		this.columns = new ArrayList<>(Arrays.asList(columns));
		locked = false;
	}

	/**
	 * 
	 * Create a matrix by performing a deep copy on another matrix.
	 * Note that this constructor will always create unlocked matrices.
	 * 
	 * @param other
	 *            The matrix to copy.
	 */
	public Matrix(Matrix other) {
		columns = new ArrayList<>();
		for (Vector column : other.columns) {
			columns.add(new Vector(column));
		}
		locked = false;
	}

	/**
	 * 
	 * Multiply every number in the matrix by a scalar value.
	 * 
	 * @param scale
	 *            The scalar value to multiply by.
	 */
	public void mult(double scale) {
		if (locked) {
			throw new IllegalAccessError();
		}

		for (Vector column : columns) {
			column.mult(scale);
		}
	}

	/**
	 * 
	 * Treating the column vectors as points on a 2d plane, this method rotates those points around
	 * the axis.
	 * 
	 * @param radians
	 *            The angle to rotate, in radians.
	 */
	public void rotate(double radians) {
		if (locked) {
			throw new IllegalAccessError();
		}

		for (Vector column : columns) {
			column.rotate(radians);
		}
	}

	/**
	 * 
	 * A convenience method that rotates vectors around a given point on a 2d plane. It handles translating
	 * the matrix, rotating it, and translating back, so that it has effectively rotated around the given
	 * point instead of the origin.
	 * 
	 * @param center
	 *            The point to rotate around.
	 * @param radians
	 *            The angle to rotate, in radians.
	 */
	public void rotateAround(Vector center, double radians) {
		if (locked) {
			throw new IllegalAccessError();
		}

		Vector undoCenter = Vector.mult(center, -1);

		transform(undoCenter);
		rotate(radians);
		transform(center);
	}

	/**
	 * 
	 * Treating the column vectors as points on a 2d plane, this method moves those points by the given
	 * vector. Specifically, the given vector is added to each column.
	 * 
	 * @param transform
	 *            The vector to add.
	 */
	public void transform(Vector transform) {
		if (locked) {
			throw new IllegalAccessError();
		}

		for (Vector column : columns) {
			column.add(transform);
		}
	}

	/**
	 * 
	 * Returns the column with the given index as a Vector.
	 * 
	 * @param index
	 *            The index of the column to return.
	 * @return The referenced column as a {@link util.Vector Vector}.
	 */
	public Vector get(int index) {
		return columns.get(index);
	}

	/**
	 * 
	 * Returns the number of columns.
	 * 
	 * @return The number of columns.
	 */
	public int size() {
		return columns.size();
	}

	/**
	 * Makes this matrix read-only. Calling a method that would normally modify the contents of the matrix will instead
	 * throw a runtime exception. Useful for using matrices as constants.
	 * 
	 * @return This matrix, for method chaining purposes.
	 */
	public Matrix lock() {
		locked = true;
		return this;
	}

	/**
	 * Returns an iterator over the columns, given as {@link util.Vector Vectors}.
	 */
	@Override
	public Iterator<Vector> iterator() {
		return columns.iterator();
	}

	@Override
	public String toString() {
		String ret = "[";
		for (Vector column : columns) {
			ret += column.toString() + ", ";
		}
		return ret + "]";
	}
}
