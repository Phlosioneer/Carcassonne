package gui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

/**
 * 
 * HDivide is a pane that draws a horizontal line with a margin on either side. It can be used
 * to visually separate panes in a {@link javafx.scene.layout.VBox VBox} or {@link javafx.scene.layout.BorderPane BorderPane}.
 * 
 * @author Phlosioneer
 *
 */
public class HDivide extends Region {

	// The canvas used to draw the horizontal line.
	private Canvas line;

	/**
	 * 
	 * Creates an HDivide. HDivide is not (currently) customizable. It will grow horizontally to fit whatever pane its put in.
	 * 
	 */
	public HDivide() {
		line = new Canvas();
		getChildren().add(line);
		setMinHeight(5);
	}

	@Override
	protected void layoutChildren() {
		// Get the graphics context of the canvas.
		GraphicsContext gc = line.getGraphicsContext2D();

		// Clear whatever was previously drawn.
		gc.clearRect(0, 0, line.getWidth(), line.getHeight());

		// Resize the canvas to fill the alloted space.
		line.setWidth(getWidth());
		line.setHeight(getHeight());

		// Get the graphics context again, just in case it's changed due to
		// the resizing.
		gc = line.getGraphicsContext2D();

		// Draw a black horizontal line.
		gc.setStroke(Color.BLACK);
		gc.beginPath();
		gc.moveTo(0, getHeight() / 2);
		gc.lineTo(getWidth(), getHeight() / 2);
		gc.closePath();
		gc.stroke();

		// Have the super-class position the canvas correctly.
		super.layoutChildren();
	}
}
