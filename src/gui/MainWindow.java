package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * 
 * @author Phlosioneer
 *
 */
public class MainWindow extends Application {

	// Reference to the main window.
	private Stage window;

	// TODO: Transition from multiple-scenes to single-scene using a pushpane.
	private Scene connectionScene;
	private GameScene gameScene;
	private PushPane pushPane;
	private Scene countsScene;

	/**
	 * 
	 * The constructor for MainWindow must be argumentless and cannot initialize any
	 * javafx objects; if any initialization has to be done, do it in {@code init()}
	 * and {@code start()}.
	 * 
	 * 
	 */
	public MainWindow() {

	}

	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * Saves the main window, and initializes each of the screens of the game.
	 */
	@Override
	public void start(Stage primaryWindow) {
		window = primaryWindow;
		gameScene = new GameScene(window);
		window.show();
	}
}
