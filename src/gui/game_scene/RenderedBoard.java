package gui.game_scene;

import java.util.ArrayList;
import gui.ContainerRegion;
import gui.game_scene.RenderedBoard.BoardSelectionModel.BoardIndex;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.game.Board;
import model.game.Tile;
import util.MatrixSelectionModel;
import util.MatrixSelectionModel.MatrixIndex;

public class RenderedBoard extends ContainerRegion {

	// private Rectangle selectionHighlightRectangle;
	private StackPane selectionPane;
	private BoardSelectionModel selectionModel;

	private GridPane grid;
	private ArrayList<ArrayList<StackPane>> tileContainers;
	private ArrayList<ArrayList<RenderedTile>> tiles;
	private Board board;

	public RenderedBoard(Board board) {
		grid = new GridPane();
		getChildren().add(grid);
		tiles = new ArrayList<>();
		tileContainers = new ArrayList<>();
		selectionModel = new BoardSelectionModel(this);
		selectionPane = new StackPane();
		selectionPane.setAlignment(Pos.CENTER);

		this.board = board;

		Rectangle selectionHighlightRectangle = new Rectangle(0, 0, Color.TRANSPARENT);
		selectionHighlightRectangle.setStroke(Color.AQUA);

		constructGrid();

		board.rowCountProperty().addListener(event->constructGrid());
		board.columnCountProperty().addListener(event->constructGrid());

		selectionModel.selectedIndexProperty().addListener((observable, oldVal, newVal)-> {
			if (oldVal != null) {
				StackPane oldStack = tileContainers.get(oldVal.getX()).get(oldVal.getY());
				oldStack.getChildren().remove(selectionPane);
			}

			if (newVal != null) {
				StackPane newStack = tileContainers.get(newVal.getX()).get(newVal.getY());
				newStack.getChildren().add(selectionPane);
			}
		});

		// Add a default selection indicator. Can be cleared by the user.
		Pane borderPane = new Pane();
		borderPane.setStyle("-fx-border-color: aqua");
		selectionPane.getChildren().add(borderPane);

	}

	public void constructGrid() {
		selectionModel.clearSelection();
		grid.getChildren().clear();
		tiles.clear();
		tileContainers.clear();

		for (int i = 0; i != board.getColumnCount(); i++) {
			ArrayList<RenderedTile> tileColumn = new ArrayList<>();
			ArrayList<StackPane> containerColumn = new ArrayList<>();
			tiles.add(tileColumn);
			tileContainers.add(containerColumn);
			for (int j = 0; j != board.getRowCount(); j++) {
				Tile current = board.getTile(i, j);
				RenderedTile render = new RenderedTile(current);
				tileColumn.add(render);
				StackPane container = new StackPane(render);
				containerColumn.add(container);
				grid.add(container, i, j);
				final int jCopy = j;
				final int iCopy = i;
				render.setOnAction(event->selectionModel.select(new BoardIndex(iCopy, jCopy, selectionModel)));
				render.addListener(event-> {
					if (getSelectionModel().getRenderedSelection() == render) {
						getSelectionModel().clearSelection();
					}
				});
			}
		}

		// Resize this pane, and all panes contaning it.
		layout();
		requestParentLayout();
	}

	public BoardSelectionModel getSelectionModel() {
		return selectionModel;
	}

	public StackPane getSelectionPane() {
		return selectionPane;
	}

	public Board getBoard() {
		return board;
	}

	@Override
	protected double computeMinWidth(double height) {
		return grid.getWidth();
	}

	@Override
	protected double computeMinHeight(double width) {
		return grid.getHeight();
	}

	public static class BoardSelectionModel extends MatrixSelectionModel<Tile, BoardSelectionModel.BoardIndex> {

		private RenderedBoard parent;
		private SimpleObjectProperty<RenderedTile> renderedSelection;

		public BoardSelectionModel(RenderedBoard parent) {
			this.parent = parent;
			renderedSelection = new SimpleObjectProperty<>();
		}

		@Override
		public void clearSelection() {
			setSelectedIndex(null);
			setSelectedItem(null);
			renderedSelection.set(null);
		}

		@Override
		public void select(BoardIndex index) {
			setSelectedIndex(index);
			setSelectedItem(parent.board.getTile(index.getX(), index.getY()));
			renderedSelection.set(parent.tiles.get(index.getX()).get(index.getY()));
		}

		@Override
		public void clearAndSelect(BoardIndex index) {
			select(index);
		}

		@Override
		public void clearAndSelect(Tile item) {
			select(item);
		}

		@Override
		public boolean isEmpty() {
			return getSelectedItem() == null;
		}

		@Override
		public void select(Tile item) {
			for (int i = 0; i != parent.board.getColumnCount(); i++) {
				for (int j = 0; j != parent.board.getRowCount(); j++) {
					if (parent.board.getTile(i, j) == item) {
						select(new BoardIndex(i, j, this));
					}
				}
			}
		}

		public ReadOnlyObjectProperty<RenderedTile> renderedSelectionProperty() {
			return renderedSelection;
		}

		public RenderedTile getRenderedSelection() {
			return renderedSelection.get();
		}

		public static class BoardIndex extends MatrixIndex {

			public BoardIndex(int x, int y, BoardSelectionModel parent) {
				super(x, y);
			}

			public int getX() {
				return x;
			}

			public int getY() {
				return y;
			}

		}

	}
}
