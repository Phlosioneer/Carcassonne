package gui.game_scene;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.game.Tile;
import util.Matrix;
import util.MatrixGeom;
import util.Vector;

public class RenderedTileRegion {

	private Matrix region;
	private TileRegionType regionType;
	private int rotation;

	public RenderedTileRegion(Matrix region, TileRegionType regionType, Tile tile) {
		this.region = region;
		this.regionType = regionType;

		if (tile != null) {
			rotation = tile.getRotation();
			tile.rotationProperty().addListener(event->rotation = tile.getRotation());
		}
	}

	public void render(GraphicsContext graphics, double tileWidth) {
		render(graphics, tileWidth, getStroke(), getFill());
	}

	private void render(GraphicsContext graphics, double tileWidth, Color stroke, Color fill) {
		if (regionType == TileRegionType.EMPTY) {
			renderEmptyTile(graphics, tileWidth);
			return;
		}

		Matrix matrix = translateMatrix(tileWidth);
		Vector start = matrix.get(0);
		graphics.beginPath();
		graphics.moveTo(start.x, start.y);
		for (int i = 1; i < matrix.size(); i++) {
			Vector current = matrix.get(i);
			graphics.lineTo(current.x, current.y);
		}
		graphics.closePath();

		graphics.setStroke(stroke);
		graphics.setFill(fill);
		graphics.fill();
		graphics.stroke();
	}

	private Matrix translateMatrix(double tileWidth) {
		Matrix matrix = new Matrix(region);
		matrix.rotateAround(new Vector(0.5, 0.5), (Math.PI / 2) * rotation);
		matrix.mult(tileWidth);
		return matrix;
	}

	private static void renderEmptyTile(GraphicsContext graphics, double width) {
		double height = width;

		graphics.setFill(Color.DARKGRAY);
		graphics.fillRect(0, 0, width, height);

		graphics.beginPath();
		graphics.setStroke(Color.GRAY);
		for (int i = 0; i < width && i < height; i += 10) {
			graphics.moveTo(0, i);
			graphics.lineTo(i, 0);
			graphics.moveTo(width, i);
			graphics.lineTo(i, height);
		}

		graphics.moveTo(0, 0);
		graphics.lineTo(width, 0);
		graphics.lineTo(width, height);
		graphics.lineTo(0, height);
		graphics.lineTo(0, 0);
		graphics.closePath();

		graphics.stroke();
	}

	private Color getStroke() {
		switch (regionType) {
			case EMPTY:
			case FARM:
				return Color.TRANSPARENT;
			case CITY:
			case SHIELD:
			case ROAD:
			case MONASTERY:
				return Color.BLACK;
			default:
				throw new IllegalStateException();
		}
	}

	private Color getFill() {
		switch (regionType) {
			case SHIELD:
				return Color.BLUE;
			case MONASTERY:
				return Color.WHITE;
			case EMPTY:
				return Color.TRANSPARENT;
			case CITY:
				return Color.BROWN;
			case ROAD:
				return Color.YELLOW;
			case FARM:
				return Color.GREEN;
			default:
				throw new IllegalStateException();
		}
	}

	public boolean contains(double x, double y, double tileWidth) {
		return MatrixGeom.matrixContainsPoint(translateMatrix(tileWidth), x, y);
	}

	public TileRegionType getType() {
		return regionType;
	}

	public void renderHighlight(GraphicsContext graphics, double width) {
		double weight = graphics.getLineWidth();
		graphics.setLineWidth(4 * weight);
		render(graphics, width, Color.BLUEVIOLET, Color.TRANSPARENT);
		graphics.setLineWidth(weight);
	}

	public static enum TileRegionType {
		EMPTY, CITY, ROAD, FARM, MONASTERY, SHIELD;
	}

	public Matrix getRegion() {
		return region;
	}
}
