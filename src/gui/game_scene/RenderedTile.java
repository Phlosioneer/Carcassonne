package gui.game_scene;

import java.io.IOException;
import java.util.ArrayList;
import gui.game_scene.RenderedTileRegion.TileRegionType;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import model.TileGraphicsFile;
import model.TileGraphicsFile.JsonGraphicType;
import model.game.Tile;
import model.game.TileType;
import util.Matrix;
import util.MatrixGeom;
import util.Vector;

public class RenderedTile extends Canvas implements Observable {

	private static final double MEEPLE_WIDTH = 10;

	private ObjectProperty<Tile> tile;
	private InvalidationListener tileListener;
	private ObjectProperty<EventHandler<MouseEvent>> onAction;
	private ArrayList<InvalidationListener> listeners;
	private ArrayList<RenderedTileRegion> regions;
	private RenderedTileRegion highlightedRegion;
	private RenderedTileRegion meepleRegion;

	public RenderedTile(Tile tile) {
		this(tile, false);
	}

	public RenderedTile(Tile tile, boolean preview) {
		super(preview ? 40 : 50, preview ? 40 : 50);
		this.tile = new SimpleObjectProperty<>();
		onAction = new SimpleObjectProperty<>();
		listeners = new ArrayList<>();
		regions = new ArrayList<>();
		highlightedRegion = null;

		// Whenever the tile changes, re-render it.
		tileListener = observable-> {
			Platform.runLater(()-> {
				rebuildRegions();
				renderTile();
				for (InvalidationListener listener : listeners) {
					listener.invalidated(this);
				}
			});
		};

		setOnMousePressed(event-> {
			EventHandler<MouseEvent> listener = onAction.get();
			if (listener != null) {
				listener.handle(event);
			}
		});

		this.tile.addListener((observable, oldVal, newVal)-> {
			// Sever connections with the old tile.
			if (oldVal != null) {
				oldVal.rotationProperty().removeListener(tileListener);
				oldVal.typePorperty().removeListener(tileListener);
			}

			// Connect listeners to new tile.
			if (newVal != null) {
				newVal.rotationProperty().addListener(tileListener);
				newVal.typePorperty().addListener(tileListener);
			}

			rebuildRegions();
			renderTile();
		});

		this.tile.set(tile);

		if (!preview) {
			setOnMouseMoved(event-> {

				if (highlightedRegion != null && highlightedRegion.contains(event.getX(), event.getY(), getWidth())) {
					return;
				}

				highlightedRegion = null;
				for (RenderedTileRegion region : regions) {
					if (region.getType() == TileRegionType.EMPTY || region.getType() == TileRegionType.SHIELD) {
						continue;
					}

					if (region.contains(event.getX(), event.getY(), getWidth())) {
						highlightedRegion = region;
						break;
					}
				}

				renderTile();
			});

			setOnMouseExited(event-> {
				highlightedRegion = null;
				renderTile();
			});

			setOnMouseClicked(event-> {
				if (highlightedRegion == null) {
					return;
				}
				if (highlightedRegion == meepleRegion) {
					rebuildRegions();
					renderTile();
					getOnMouseMoved().handle(event);
				} else {
					meepleRegion = highlightedRegion;
					renderTile();
				}
			});
		}
	}

	public ObjectProperty<Tile> tileProperty() {
		return tile;
	}

	public void setTile(Tile newTile) {
		tile.set(newTile);

	}

	public Tile getTile() {
		return tile.get();
	}

	public void rebuildRegions() {

		highlightedRegion = null;
		meepleRegion = null;
		regions.clear();

		if (getTile().getType() == TileType.EMPTY) {
			regions.add(new RenderedTileRegion(null, TileRegionType.EMPTY, getTile()));
			return;
		}

		TileType tileType = getTile().getType();

		TileGraphicsFile graphicsFile = TileGraphicsFile.getTileGraphicsFile();
		ArrayList<Matrix> grassGraphics;
		try {
			grassGraphics = graphicsFile.getTileMatrix(tileType, JsonGraphicType.GRASS);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in renderTile of RenderedTile.", e1);
		}
		for (Matrix grass : grassGraphics) {
			regions.add(new RenderedTileRegion(grass, TileRegionType.FARM, getTile()));
		}

		ArrayList<Matrix> roadGraphics;
		try {
			roadGraphics = graphicsFile.getTileMatrix(tileType, JsonGraphicType.ROAD);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in renderTile of RenderedTile.", e);
		}
		for (Matrix road : roadGraphics) {
			regions.add(new RenderedTileRegion(road, TileRegionType.ROAD, getTile()));
		}

		ArrayList<Matrix> cityGraphics;
		try {
			cityGraphics = graphicsFile.getTileMatrix(tileType, JsonGraphicType.CITY);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in renderTile of RenderedTile.", e);
		}
		for (Matrix city : cityGraphics) {
			regions.add(new RenderedTileRegion(city, TileRegionType.CITY, getTile()));
		}

		ArrayList<Matrix> shieldGraphics;
		try {
			shieldGraphics = graphicsFile.getTileMatrix(tileType, JsonGraphicType.SHIELD);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in renderTile of RenderedTile.", e);
		}
		for (Matrix shield : shieldGraphics) {
			regions.add(new RenderedTileRegion(shield, TileRegionType.SHIELD, getTile()));
		}

		ArrayList<Matrix> monasteryGraphics;
		try {
			monasteryGraphics = graphicsFile.getTileMatrix(tileType, JsonGraphicType.MONASTERY);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in renderTile of RenderedTile.", e);
		}
		for (Matrix monastery : monasteryGraphics) {
			regions.add(new RenderedTileRegion(monastery, TileRegionType.MONASTERY, getTile()));
		}
	}

	public void renderTile() {
		GraphicsContext graphics = getGraphicsContext2D();

		double width = getWidth();
		double height = getHeight();
		if ((int) width != (int) height) {
			throw new IllegalStateException();
		}

		graphics.beginPath();
		graphics.closePath();
		graphics.clearRect(0, 0, width, height);

		if (getTile().getType() == TileType.EMPTY) {
			rebuildRegions();

		} else {
			graphics.setFill(Color.AQUA);
			graphics.fillRect(0, 0, width, height);
		}
		for (RenderedTileRegion region : regions) {
			region.render(graphics, width);
		}

		if (highlightedRegion != null) {
			highlightedRegion.renderHighlight(getGraphicsContext2D(), getWidth());
		}

		if (meepleRegion != null) {
			Vector center = MatrixGeom.centerPoint(meepleRegion.getRegion());
			drawWithMeeple(getGraphicsContext2D(), center, Color.BLACK, Color.BLACK);
		}
	}

	private void drawWithMeeple(GraphicsContext graphics, Vector meepleInput, Color stroke, Color fill) {
		Vector meeple = new Vector(meepleInput);
		meeple.rotateAround(new Vector(0.5, 0.5), (Math.PI / 2) * getTile().getRotation());
		meeple.mult(getWidth());

		graphics.beginPath();
		graphics.arc(meeple.x, meeple.y, MEEPLE_WIDTH / 2, MEEPLE_WIDTH / 2, 0, 360);
		graphics.closePath();

		graphics.setStroke(stroke);
		graphics.setFill(fill);
		graphics.fill();
		graphics.stroke();
	}

	public ObjectProperty<EventHandler<MouseEvent>> onActionProperty() {
		return onAction;
	}

	public void setOnAction(EventHandler<MouseEvent> handler) {
		onAction.set(handler);
	}

	public EventHandler<MouseEvent> getOnAction() {
		return onAction.get();
	}

	@Override
	public void addListener(InvalidationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		listeners.remove(listener);
	}
}
