package gui.game_scene;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Queue;
import gui.ContainerRegion;
import gui.game_scene.RenderedBoard.BoardSelectionModel.BoardIndex;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.game.Tile;
import model.game.TileType;

public class ControlPanel extends ContainerRegion {

	private static final double PIECE_COUNT_WINDOW_HEIGHT = 500;

	private RenderedTile renderedTile;

	@FXML
	private Button placeTileButton;
	// private Button placeGuyButton;
	// private Button rotateButton;
	// private Button pieceCountButton;
	private Stage pieceCountWindow;
	private HBox root;

	@FXML
	private HBox renderedTileBox;

	private RenderedBoard board;
	private Queue<TileType> deck;

	private RenderedTile miniTile;

	public ControlPanel(RenderedBoard board, Queue<TileType> deck) {
		this.board = board;
		this.deck = deck;
		pieceCountWindow = null;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ControlPanel.fxml"));
			// loader.setRoot(this);
			loader.setController(this);
			root = loader.load();
			getChildren().add(root);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("IOException handler not yet written in ControlPanel of ControlPanel.", e);
		}

		renderedTile = new RenderedTile(new Tile(deck.poll()));
		renderedTileBox.getChildren().add(renderedTile);

		// Set up the preview tile.
		miniTile = new RenderedTile(renderedTile.getTile(), true);
		renderedTile.tileProperty().addListener((observable, oldVal, newVal)-> {
			miniTile.setTile(newVal);
		});

		board.getSelectionModel().selectedItemProperty().addListener(this::doSelectionChanged);
	}

	public void onPlaceTile(ActionEvent event) {
		BoardIndex index = board.getSelectionModel().getSelectedIndex();
		boolean success = board.getBoard().getTile(index.getX(), index.getY()).copyTile(renderedTile.getTile());
		if (success) {
			TileType next = deck.poll();
			if (next == null) {
				renderedTile.setTile(new Tile());
				// TODO: Handle game-over.
			} else {
				renderedTile.setTile(new Tile(next));
			}
		}
	}

	public void onRotate(ActionEvent event) {
		renderedTile.getTile().rotate();
	}

	public void onPieceCounts(ActionEvent event) {
		if (pieceCountWindow != null) {
			pieceCountWindow.show();
			pieceCountWindow.requestFocus();
		} else {
			pieceCountWindow = new Stage();
			GridPane grid = new GridPane();
			ScrollPane root = new ScrollPane(grid);
			root.setFitToWidth(true);
			root.setFitToHeight(false);
			root.setMaxHeight(PIECE_COUNT_WINDOW_HEIGHT);
			pieceCountWindow.setScene(new Scene(root));

			Map<TileType, Integer> tileCounts = getTileCounts();
			for (int i = 0; i != TileType.values().length; i++) {
				TileType currentType = TileType.values()[i];
				RenderedTile currentTile = new RenderedTile(new Tile(currentType));
				Integer count = tileCounts.get(currentType);
				Label countLabel = new Label(count.toString());
				grid.addRow(i, currentTile, countLabel);
			}

			pieceCountWindow.show();
		}
	}

	private Map<TileType, Integer> getTileCounts() {
		EnumMap<TileType, Integer> ret = new EnumMap<>(TileType.class);

		for (TileType current : TileType.values()) {
			ret.put(current, 0);
		}

		for (TileType current : deck) {
			ret.put(current, ret.get(current) + 1);
		}

		return ret;
	}

	public void onPlaceGuy(ActionEvent event) {
		// TODO: Implement meeples.
	}

	private void doSelectionChanged(ObservableValue<? extends Tile> observable, Tile oldVal, Tile newVal) {
		if (newVal != null) {
			// If this isn't an empty tile, then don't display a preview of the new tile.
			// If the current tile is empty, the deck is empty and the game is over, so
			// disable the place button and don't preview anything.
			if (newVal.getType() == TileType.EMPTY && renderedTile.getTile().getType() != TileType.EMPTY) {
				if (!board.getSelectionPane().getChildren().contains(miniTile)) {
					board.getSelectionPane().getChildren().add(miniTile);
				}
				placeTileButton.setDisable(false);
			} else {
				board.getSelectionPane().getChildren().remove(miniTile);
				placeTileButton.setDisable(true);
			}
		} else {
			placeTileButton.setDisable(true);
		}
	}
}
