package gui;

import gui.game_scene.ControlPanel;
import gui.game_scene.RenderedBoard;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.game.GameState;

/**
 * 
 * GameScene manages all the views and controls that are present while playing a game.
 * 
 * @author Phlosioneer
 *
 */
public class GameScene {

	private Scene mainScene;
	private Label playerOneScoreLabel;
	private Label playerTwoScoreLabel;
	private GameState game;

	private BorderPane root;
	private Stage window;

	// TODO: Refactor GameScene to be a ContainerRegion.
	/**
	 * 
	 * Creates a GameScene, and adds the created scene to {@code window}.
	 * 
	 * @param window
	 *            The main window.
	 */
	public GameScene(Stage window) {

		// Save the window.
		this.window = window;

		// Use a borderpane as the root pane.
		root = new BorderPane();
		mainScene = new Scene(root);
		window.setScene(mainScene);

		// The top bar contains the score of each player
		// TODO: Pull this out into its own object.
		VBox topAndDivider = new VBox();
		AnchorPane scoreBar = new AnchorPane();
		topAndDivider.getChildren().add(scoreBar);
		root.setTop(topAndDivider);

		HBox playerOnePane = new HBox();
		scoreBar.getChildren().add(playerOnePane);
		playerOnePane.getChildren().add(new Label("Player 1: "));
		playerOneScoreLabel = new Label("0");
		playerOnePane.getChildren().add(playerOneScoreLabel);
		AnchorPane.setLeftAnchor(playerOnePane, 0.0);

		HBox playerTwoPane = new HBox();
		scoreBar.getChildren().add(playerTwoPane);
		playerTwoPane.getChildren().add(new Label("Player 2: "));
		playerTwoScoreLabel = new Label("0");
		playerTwoPane.getChildren().add(playerTwoScoreLabel);
		AnchorPane.setRightAnchor(playerTwoPane, 0.0);

		// Create a dividor between the top and center areas.
		topAndDivider.getChildren().add(new HDivide());

		// Create the GameState object, which manages the rules of the game.
		game = new GameState();

		// Render the board in the center of the window.
		// TODO: Adjust the size of GameScene as the RenderedBoard grows.
		RenderedBoard board = new RenderedBoard(game.getBoard());
		HBox centeredBoardX = new HBox();
		centeredBoardX.setAlignment(Pos.CENTER);
		centeredBoardX.getChildren().add(board);
		VBox centeredBoardY = new VBox();
		centeredBoardY.setAlignment(Pos.CENTER);
		centeredBoardY.getChildren().add(centeredBoardX);
		root.setCenter(centeredBoardY);

		// Make a control panel and a dividor between the bottom and the center areas.
		VBox bottomAndDivider = new VBox();
		bottomAndDivider.getChildren().add(new HDivide());
		ControlPanel gameControls = new ControlPanel(board, game.getTileDeck());
		bottomAndDivider.getChildren().add(gameControls);
		root.setBottom(bottomAndDivider);

		// Whenever the bounds for the borderpane are changed, change the window's minimum bounds too.
		root.boundsInParentProperty().addListener(this::onBoundsChange);
	}

	/**
	 * 
	 * This is called whenever the bounds of BorderPane change. It sets the minimum size of the window
	 * to match the minimum size of this view's bounds.
	 * 
	 * @param observable
	 *            Ignored.
	 * @param oldVal
	 *            Ignored.
	 * @param newVal
	 *            The new bounds of the BorderPane.
	 */
	private void onBoundsChange(ObservableValue<? extends Bounds> observable, Bounds oldVal, Bounds newVal) {
		double minWidth = root.minWidth(newVal.getHeight());
		double minHeight = root.minHeight(newVal.getWidth());
		double vertInset = window.getHeight() - mainScene.getHeight();
		double horizInset = window.getWidth() - mainScene.getWidth();
		window.setMinWidth(minWidth + horizInset);
		window.setMinHeight(minHeight + vertInset);
	}
}
